<?php

require_once(dirname(__FILE__) . '/vendor/autoload.php');
require_once(dirname(__FILE__) . '/config.php');

use pimax\FbBotApp;
use pimax\Messages\Message;
use pimax\Messages\ImageMessage;
use pimax\UserProfile;
use pimax\Messages\MessageButton;
use pimax\Messages\StructuredMessage;
use pimax\Messages\MessageElement;
use pimax\Messages\MessageReceiptElement;
use pimax\Messages\Address;
use pimax\Messages\Summary;
use pimax\Messages\Adjustment;

$database_filename = "followers.sqlite";
$database_file_exists = file_exists($database_filename);

try {
    # SQLite  
    $DBH = new PDO("sqlite:$database_filename");
    $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

    if (!$database_file_exists) {
          $DBH->exec("CREATE TABLE IF NOT EXISTS followers (
                      id INTEGER PRIMARY KEY AUTOINCREMENT, 
                      facebook_id TEXT)");
    }
    
    /*
     *  Make Bot Instance
     */
    $bot = new FbBotApp($page_token);
    
    /*
     *  When receive something
     */
    if (!empty($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe' && $_REQUEST['hub_verify_token'] == $verify_token) {

        /*
         *  Webhook setup request
         */
        echo $_REQUEST['hub_challenge'];

    } else {

        /*
         *  Other event
         */

        $data = json_decode(file_get_contents("php://input"), true, 512, JSON_BIGINT_AS_STRING);

        file_put_contents("result_fb.txt", print_r($data, true));

        if (!empty($data['entry'][0]['messaging'])) {
            
            foreach ($data['entry'][0]['messaging'] as $message) {

                /*
                 *  Skipping delivery messages
                 */
                if (!empty($message['delivery'])) {
                    continue;
                }

                $command = "";

                /*
                 *  When bot receive message from user
                 */
                if (!empty($message['message'])) {
                    $command = $message['message']['text'];

                /*
                 *  When bot receive button click from user
                 */
                } else if (!empty($message['postback'])) {
                    $command = $message['postback']['payload'];
                }
                
                /*
                 * Check user status (followed or not):
                 */
                $sender_id = $message['sender']['id'];
                
                $STH = $DBH->prepare("SELECT * FROM followers WHERE facebook_id = :sender_id");
                $STH->bindParam(":sender_id", $sender_id);
                $STH->execute();
                
                $user_followed = $STH->fetch() ? true : false;
                
                /*
                 *  Handle command
                 */
                switch ($command) {
                    // When user push "Get started" button:
                    case 'Get started':

                        if (!$user_followed) {
                            $bot->send(new StructuredMessage($message['sender']['id'],
                                StructuredMessage::TYPE_GENERIC,
                                [
                                    'elements' => [
                                        new MessageElement("Hi! Want to subscribe to our news channel?", "", "", [
                                            new MessageButton(MessageButton::TYPE_POSTBACK, 'Yes, subscribe me!'),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, 'No, thank you.'),
                                        ]),
                                    ]
                                ]
                            ));
                        }

                        break;

                    // When bot receive "Yes, subscribe me!"
                    case 'Yes, subscribe me!':
                        if (!$user_followed) {
                            $STH = $DBH->prepare("INSERT INTO followers (facebook_id) VALUES (:sender_id)");
                            $STH->bindParam(":sender_id", $sender_id);
                            if ($STH->execute()) {
                                $bot->send(new Message($message['sender']['id'], 'You have been subscribed! Thank you!'));
                            }
                        }
                        break;
                        
                    // When bot receive "Yes, unsubscribe me!"
                    case 'Yes, unsubscribe me!':
                        if ($user_followed) {
                            $STH = $DBH->prepare("DELETE FROM followers WHERE facebook_id = :sender_id");
                            $STH->bindParam(":sender_id", $sender_id);
                            if ($STH->execute()) {
                                $bot->send(new Message($message['sender']['id'], 'You have been unsubscribed!'));
                            }
                        }
                        break;

                    // When bot receive "No, thank you."
                    case 'No, thank you.':
                        $bot->send(new Message($message['sender']['id'], 'OK! Come back when you change your mind!'));
                        break;

                    // Other message received
                    default:
                        if (!$user_followed) {
                            $bot->send(new StructuredMessage($message['sender']['id'],
                                StructuredMessage::TYPE_GENERIC,
                                [
                                    'elements' => [
                                        new MessageElement("Sorry. I don’t understand you. Maybe you want to subscribe to our news channel?", "", "", [
                                            new MessageButton(MessageButton::TYPE_POSTBACK, 'Yes, subscribe me!'),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, 'No, thank you.'),
                                        ]),
                                    ]
                                ]
                            ));
                        } else {
                            $bot->send(new StructuredMessage($message['sender']['id'],
                                StructuredMessage::TYPE_GENERIC,
                                [
                                    'elements' => [
                                        new MessageElement("Sorry. I don’t understand you. Maybe you want to unsubscribe?", "", "", [
                                            new MessageButton(MessageButton::TYPE_POSTBACK, 'Yes, unsubscribe me!'),
                                            new MessageButton(MessageButton::TYPE_POSTBACK, 'No, thank you.'),
                                        ]),
                                    ]
                                ]
                            ));
                        }
                }
            }
        }
    }   
}

catch(PDOException $e) {  
    file_put_contents("errors.txt", $e->getMessage());
}